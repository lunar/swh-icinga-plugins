Software Heritage - Python module template
==========================================

Python module template, used as skeleton to create new modules.


.. toctree::
   :maxdepth: 2
   :caption: Contents:


.. only:: standalone_package_doc

   Indices and tables
   ------------------

   * :ref:`genindex`
   * :ref:`modindex`
   * :ref:`search`
